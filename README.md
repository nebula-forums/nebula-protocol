# Nebula Protocol

The protocol the Nebula network operates upon.

# TODO

**Most important TODO**
- [ ] Create documentation that is easy to understand, yet provides the necessary technical depth for someone to implement the protocol.
- [ ] Finish protocol version 1.0
# 
- [ ] Overview of how a network using the Nebula protocol operates
- [ ] Code of conduct for data sent along the Nebula network
- [ ] Forum categories
    - [ ] Politics
    - [ ] The other ones
    - [ ] Submitting a proposal for a new one
# 
- [ ] Network layout
    - [ ] Peers, ultrapeers, and nodes
        - [ ] Communication
            - [ ] Peers <-> Ultrapeers
            - [ ] Ultrapeers <-> Ultrapeers
            - [ ] Ultrapeers <-> Nodes
            - [ ] Nodes <-> Nodes
        - [ ] Functions
            - **Moved to their own parent category to prevent insane indentation**
- [ ] Content transfered over the network
    - [ ] The Content Chain
        - [ ] Overview
        - [ ] Blocks
        - [ ] Metablocks
        - [ ] Content Blocks
            - [ ] Core concepts
            - [ ] Root Block
                - [ ] Meta information
            - [ ] Diff Block
                - [ ] Meta information
                - [ ] Diff structure
            - [ ] Special Blocks
                - [ ] Hidden Block
                    - [ ] Meta information
                - [ ] Image Reference Block
                    - [ ] Meta information
        - [ ] Network transfer of blocks
    - [ ] Posts / Comments
        - [ ] Limitations
        - [ ] Meta Information
        - [ ] Text formatting
    - [ ] Images
        - [ ] Limitations
        - [ ] Image Reference
        - [ ] Image Chains
            - [ ] Meta Information
            - [ ] Content format
    - [ ] Message
        - [ ] Limitations
        - [ ] Meta information
        - [ ] Security
- [ ] Standard responses
    - [ ] Limitations
    - [ ] Routing responses
        - [ ] When only identifier is available
        - [ ] When IP is available
- [ ] Nodes
    - [ ] Peer / Ultrapeer connection proxying
    - [ ] Content distribution through the network
    - [ ] Managed forum hosting
- [ ] Peers
    - [ ] Ultrapeers
    - [ ] Mechanisms for finding other Peers / Ultrapeers
- [ ] Content transfer through the network
    - [ ] Posts / Comments / Image References
    - [ ] Images
- [ ] Requesting content
    - [ ] Limitations
- [ ] Identities
    - [ ] Purpose
    - [ ] Basic Creation (no hash)
    - [ ] Identity Hash
        - [ ] Purpose
        - [ ] Mechanism
        - [ ] Zero requirement
- [ ] "Private" messaging
    - [ ] Limitations
    - [ ] Message transmission through the network